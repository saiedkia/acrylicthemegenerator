﻿using System.Text;
using Windows.ApplicationModel.DataTransfer;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Media;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace AcrylicThemeGenerator
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        AcrylicBrush brush = new AcrylicBrush();
        public MainPage()
        {
            this.InitializeComponent();
            brush.BackgroundSource = AcrylicBackgroundSource.HostBackdrop;
        }

        private void ColorPicker_ColorChanged(ColorPicker sender, ColorChangedEventArgs args)
        {
            if (tMode.IsOn)
                brush.FallbackColor = colorPicker.Color;
            else
                brush.TintColor = colorPicker.Color;


            Background = brush;
            update();

        }

        private void TintOpacitySlider_ValueChanged(object sender, RangeBaseValueChangedEventArgs e)
        {
            brush.TintOpacity = tintOpacitySlider.Value;
            update();
        }

        private void BrushOpacitySlider_ValueChanged(object sender, RangeBaseValueChangedEventArgs e)
        {
            brush.Opacity = brushOpacitySlider.Value;
            update();
        }

        private void update()
        {
            StringBuilder b = new StringBuilder();
            b.AppendLine("<AcrylicBrush x:Key=\"acrylicBrush\" ");
            b.AppendLine($"BackgroundSource = \"{brush.BackgroundSource}\" ");
            b.AppendLine($"TintColor = \"{brush.TintColor}\" ");
            b.AppendLine($"FallbackColor = \"{brush.FallbackColor}\" ");
            b.AppendLine($"TintOpacity = \"{brush.TintOpacity.ToString("f")}\" ");
            b.AppendLine($"Opacity = \"{brush.Opacity.ToString("f")}\" /> ");

            outputText.Text = b.ToString();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DataPackage dataPackage = new DataPackage();
            dataPackage.SetText(outputText.Text);
            Clipboard.SetContent(dataPackage);
        }

        private void tMode_Toggled(object sender, RoutedEventArgs e)
        {
            tintOpacitySlider.IsEnabled = brushOpacitySlider.IsEnabled = !tMode.IsOn;
        }
    }
}
